import { Controller, Inject, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { ClientKafka, MessagePattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';
import dotenv = require('dotenv');

dotenv.config(); // configure dotenv explicitly

@Controller()
export class AppController {
  constructor(
    @Inject('KAFKA_SERVICE')
    private client: ClientKafka,
    private readonly appService: AppService,
  ) {}

  onModuleInit() {
    const topic = process.env.KAFKA_TOPIC; // kafka topic
    // required when implementing the message pattern
    this.client.subscribeToResponseOf(topic);
  }

  // method receives incoming Kafka messages and processes it
  @MessagePattern(process.env.KAFKA_TOPIC)
  public async sms(@Payload() message: any): Promise<void> {
    console.log(message);
  }

  // method handles delivery receipt webhook
  @Post('dr/webhook')
  dr_webhook(@Req() req: Request, @Res() res: Response) {
    this.appService.dr_reply(req.body);
    res.sendStatus(200); // reply with status code 200(required by vonage)
  }

  // method handles inbound message webhook
  @Post('im/webhook')
  im_webhook(@Req() req: Request, @Res() res: Response) {
    console.log(req.body);
    res.sendStatus(200);
  }
}
