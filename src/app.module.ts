import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CompressionTypes } from '@nestjs/microservices/external/kafka.interface';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ClientsModule.register([
      {
        name: 'KAFKA_SERVICE',
        transport: Transport.KAFKA, // transport layer
        options: {
          client: {
            clientId: 'client' + Math.random(),
            brokers: [process.env.KAFKA_BROKER], // seed broker
            ssl: true, // enable ssl
            sasl: {
              mechanism: 'plain', // authentication mechanism over ssl
              username: process.env.CONFLUENT_API_KEY, // username for authentication
              password: process.env.CONFLUENT_API_SECRET, // password for authentication
            },
          },
          consumer: {
            groupId: 'consumer' + Math.random(),
          },
          producer: {
            retry: {
              initialRetryTime: 100,
              retries: 8,
            },
          },
          send: {
            timeout: 50000,
            compression: CompressionTypes.GZIP,
          },
        },
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
