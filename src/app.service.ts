import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { Producer } from '@nestjs/microservices/external/kafka.interface';

@Injectable()
export class AppService implements OnModuleInit {
  private kafkaProducer: Producer;

  constructor(
    @Inject('KAFKA_SERVICE')
    private clientKafka: ClientKafka,
  ) {}

  async onModuleInit() {
    this.kafkaProducer = await this.clientKafka.connect();
  }

  // method receives data from webhook handler method and sends data to a new KAFKA_TOPIC
  public async dr_reply(data: any): Promise<void> {
    await this.kafkaProducer.send({
      topic: process.env.KAFKA_TOPIC + '.reply', // new KAFKA_TOPIC
      messages: [
        {
          key: 'payload',
          value: JSON.stringify(data),
        },
      ],
    });
  }
}
