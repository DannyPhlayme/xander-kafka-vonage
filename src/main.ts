import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { CompressionTypes } from '@nestjs/microservices/external/kafka.interface';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    // configure microservice to connect to kafka
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: 'client' + Math.random(),
        brokers: [process.env.KAFKA_BROKER], // seed broker
        ssl: true, // enable ssl
        sasl: {
          mechanism: 'plain', // authentication mechanism over ssl
          username: process.env.CONFLUENT_API_KEY, // username for authentication
          password: process.env.CONFLUENT_API_SECRET, // password for authentication
        },
        connectionTimeout: 3000,
        requestTimeout: 25000,
        retry: {
          initialRetryTime: 100,
          retries: 3,
        },
      },
      consumer: {
        groupId: 'consumer' + Math.random(),
      },
      producer: {
        retry: {
          initialRetryTime: 100,
          retries: 8,
        },
      },
      send: {
        timeout: 50000,
        compression: CompressionTypes.GZIP,
      },
    },
  });

  // start and bind microservice asynchronously to app
  await app.startAllMicroservices();
  await app.listen(3333);
}
bootstrap();
